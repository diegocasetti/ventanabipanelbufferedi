import java.awt.*;
import java.awt.event.*;
import rotordecolores.RotorDeColores;
public class Panel12Btn extends Panel implements ActionListener {
    private Button b[];
    private VentanaBiPanel suVta;
    private Color colorUltimoClic;
    private int indiceUltimoClick;
    public Panel12Btn(VentanaBiPanel padre) {
        super();
        this.suVta = padre;
        RotorDeColores rot = new RotorDeColores();
        this.b = new Button[12];
        this.setLayout(new GridLayout(12,1));
        rot.setColorName(0,"Fucsia");
        for(int i=0;i<12;i++,rot.next()) {
            this.b[i]=new Button(rot.getCurrentColorAsString());
            this.b[i].setActionCommand(i+"");
            this.b[i].setBackground(rot.getCurrentColor());
            this.b[i].addActionListener(this);
            this.add(this.b[i]);
        }
        this.setVisible(true);
        this.colorUltimoClic = b[0].getBackground();
        this.indiceUltimoClick=0;
    }
    
    
    public void actionPerformed(ActionEvent e) {
        Object clikeado = e.getSource();
        if(clikeado.getClass().equals(this.b[0].getClass())) {
            Button x = (Button)clikeado;
            this.colorUltimoClic = x.getBackground();
            this.suVta.fijarColor(this,x);
            
            this.indiceUltimoClick = Integer.parseInt(x.getActionCommand());
            //this.suVta.pintaCentro(this.colorUltimoClic);
            this.suVta.pintar(this.ultimoColor());
        }
    }
    
    public int getUltimoClic() {
        return this.indiceUltimoClick;
    }
    
    public Color ultimoColor() {
        return this.colorUltimoClic;
    }
}
