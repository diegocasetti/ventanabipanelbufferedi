import java.awt.*;
public class PuntoColor extends Point {
    private Color micol;
    public PuntoColor(int px,int py,Color col){
        this.setLocation(px, py);
        this.micol = col;
    }
    
    public Color getColor(){
        return micol;
    }
}
