import java.awt.*;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.awt.geom.GeneralPath;
import javax.swing.*;
import java.util.*;
public class CanvasDraw extends Canvas {
    BufferedImage img;
    int px,py;
    Color color;
    public CanvasDraw() {
        super();
        
    }
    
    public void pintaLinea(int ori,int des,Color col){
            if (this.img==null)
                this.img = new BufferedImage(this.getWidth(),this.getHeight(),BufferedImage.TYPE_INT_RGB);
            this.px=ori;
            this.py=des;
            this.color =col;
            repaint(); 
    }

    public void paint(Graphics g){
        int h = this.getHeight()/12;
        int w = this.getWidth();
        this.reescalasiacaso();
        Graphics gra = this.img.getGraphics();
        gra.setColor(this.color);
        gra.drawLine(0, h*px+h/2, w, h*py+h/2);
        g.drawImage(this.img,0,0,this);        
    }
    
    public void reescalasiacaso(){
        Image i;
        if(this.getHeight()!=this.img.getHeight() || 
           this.getWidth()!= this.img.getWidth() ) {
             i = this.img.getScaledInstance(this.getHeight(), this.getWidth(), Image.SCALE_SMOOTH);
             this.img = new BufferedImage(this.getWidth(),this.getHeight(),BufferedImage.TYPE_INT_RGB);
             this.img.getGraphics().drawImage(i, 0, 0, this);
        }
        
    }
}
