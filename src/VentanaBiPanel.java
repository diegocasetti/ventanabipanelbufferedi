import java.awt.*;
import java.awt.event.*;
public class VentanaBiPanel extends Frame implements WindowListener {
    Panel12Btn pnlIzq,pnlDer;
    private CanvasDraw img;
    
    
    //Constructor
    public VentanaBiPanel() {
        super();
        this.setSize(300, 250);
        this.pnlIzq = new Panel12Btn(this);
        this.pnlDer = new Panel12Btn(this);
        this.img = new CanvasDraw();
        //this.img.setBackground(pnlIzq.ultimoColor());
        this.setLayout(new BorderLayout());
        this.addWindowListener(this);
        this.add("East",this.pnlIzq);
        this.add("West",this.pnlDer);
        this.add("Center",this.img);
        this.setVisible(true);
    }
    
    public void pintar(Color col) {
        int ori = this.pnlIzq.getUltimoClic();
        int des = this.pnlDer.getUltimoClic();
        this.img.pintaLinea(des,ori,col);
    }

    public void pintaCentro(Color col){
        this.img.setBackground(col);
    }
    
    public void fijarColor(Panel12Btn pan,Button b) {
        if(pan.equals(this.pnlDer))
            b.setBackground(this.pnlIzq.ultimoColor());
        else if(pan.equals(this.pnlIzq))
            b.setBackground(this.pnlDer.ultimoColor());
        
    }
    
    public void windowOpened(WindowEvent e){}

    public void windowClosing(WindowEvent e){
        System.exit(0);
    }

    public void windowClosed(WindowEvent e){}

    public void windowIconified(WindowEvent e){}

    public void windowDeiconified(WindowEvent e){}

    public void windowActivated(WindowEvent e){}

    public void windowDeactivated(WindowEvent e){}
    
    
}
