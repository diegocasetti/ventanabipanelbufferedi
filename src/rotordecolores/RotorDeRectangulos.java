package rotordecolores;
import java.awt.Rectangle;
public class RotorDeRectangulos {
    private Rectangle r[];
    private int actual = 0;
    public RotorDeRectangulos() {
        r = new Rectangle[6];
        for (int k=0;k<6;k++) {
            r[k]=new Rectangle(k,k+2,50,100+k);
        }
    }
    
    public void next() {
        this.actual++;
        if (this.actual==6) this.actual=0;
    }
    
    public Rectangle getCurrentRectangle() {
        Rectangle ret = new Rectangle(this.r[this.actual]);
        return ret;
    }
    
    public String getCurrentRectangleAsString() {
        Rectangle q = this.r[this.actual];
        return "[ "+q.x+","+q.y+","+q.height+","+q.width+" ]";
    }
    
    public double getArea() {
        Rectangle q = this.r[this.actual];
        return (double)(q.height*q.width);
    }
}
