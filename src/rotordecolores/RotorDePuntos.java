package rotordecolores;
import java.awt.Point;
public class RotorDePuntos {
    private Point p[];
    private int actual = 0;
    public RotorDePuntos() {
        p = new Point[10];
        for (int k=0;k<10;k++) {
            p[k]=new Point(k+2,k*k);
        }
    }
    
    public void next() {
        this.actual++;
        if (this.actual==10) this.actual=0;
    }
    
    public Point getCurrentPoint() {
        Point ret = new Point(this.p[this.actual]);
        return ret;
    }
    
    public String getCurrentPointAsString() {
        return "[ "+this.p[this.actual].x+","+this.p[this.actual].y+" ]";
    }
}
