package rotordecolores;
import java.awt.Color;
public class RotorDeColores {
    private Color r[];
    private String n[];
    private int actual = 0;
    public RotorDeColores() {
        r = new Color[6];
        n = new String[6];
        r[0]= new Color(16711935);
        n[0]= "Magenta";
        r[1]=new Color(7048739);
        n[1]="Verde Oliva";
        r[2]=new Color(16744272);
        n[2]= "Coral";
        r[3]=new Color(8388564);
        n[3]= "Aguamarina";
        r[4]=new Color(4251856);
        n[4]= "Turquesa";
        r[5]= new Color(4915330);
        n[5]= "Indigo";

    }
    
    public boolean setColorName(int indice,String nuevoNom){
        boolean ret=true;
        if(indice>=0 && indice<=5) {
            this.n[indice]=nuevoNom;
        }
        else ret=false;
        return ret;
    }
    
    public void next() {
        this.actual++;
        if (this.actual==6) this.actual=0;
    }
    
    public Color getCurrentColor() {
        Color c = new Color(r[this.actual].getRGB());
        return c;
    }
    
    public String getCurrentColorAsString() {
        return n[this.actual];
    }
}
